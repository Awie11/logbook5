 Head | Information |
| ------ | ----------- |
| Logbook entry number | 05 |
| Name  | Kee Soon Win |
| Matrix no.  | 195901 |
| Year  | 4 |
| Subsystem  | Simulation |
| Group  | 7 |


## Tables

| Target | Information |
| ------ | ----------- |
| Agenda  | -Do CFD one more time for the design part.<br> -Parameters of coefficient of lift (Cl), coefficient of drag(Cd) and moment coeffiecient(Cm) by using Ansys software.<br> -Retrieve data from other subsystem group to calculate weight.<br> -To calculate the volume of the airship by using the parameter that have been retrieve from other subsystem.|
| Goals | -To get the weight of the airship from the volume of the airship.<br> -From the formula that we have found in the book volume and the weight are related to each other so we can calculate for that part.<br> -To calculate the Bouyancy Force, Aerodynamic Lift and Bouyancy Ratio |
| Problems | -Having problem to come out with Cl and Cd due to Ansys software take time to come out with the result<br> -A little bit confusing during the calculation part of volume and weight |
| Decision | -To do manual calculation to get the value for volume and weight.<br> -To carry out the Bouyancy Lift, Bouyancy Ratio and the Aerodynamic Lift by referring to the Fundamentals of the Airship and Design book.  |
| Method | -Using spheroid formula to calculate the volume of the airship.<br> -After collect all the data we calculate the total weight of the airship.<br> Note: real gross weight will update soon after we get the the weight of the skin|
| Justification | -we have plot the Volume Vs. Weight graph<br> -Able to know the approximate bouyancy lift, aerodynamic lift and the bouyancy ratio. |
| Impact | -Once the Volume vs weight graph have been plot we can see the relationship between volume and weight more clearly. |
| Next step | -To calculate and plot the relation of length vs lift force |
